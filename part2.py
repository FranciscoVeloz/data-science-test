import pandas as pd
import numpy as np

# Load csv file data
data = pd.read_csv('Cellphone.csv')

# Transform 'Price' to a tag vector
y = data['Price'].values.astype(float)

# Select the features that want use for the regression
selected_features = ['resoloution', 'ppi', 'cpu freq', 'internal mem', 'ram', 'RearCam', 'Front_Cam', 'battery', 'thickness']
X = data[selected_features].values.astype(float)

# Add a column of ones for the intercept term
X = np.c_[np.ones(X.shape[0]), X]

## Linear Regression with Ordinary Least Squares (OLS)
# Calculate model coefficients using OLS
coefficients = np.linalg.inv(X.T @ X) @ X.T @ y

# The first element of 'coefficients' is the intercept term, and the rest are the characteristic coefficients
intercept, feature_coefficients = coefficients[0], coefficients[1:]

print("intercept OLS", intercept)
print("feature_coefficients OLS", feature_coefficients)

## Linear Regression with Gradient Descent
# Define hyperparameters
learning_rate = 0.01
num_iterations = 10

# Initialize coefficients randomly or with zeros
coefficients = np.random.rand(X.shape[1])

# Implement Gradient Descent
for iteration in range(num_iterations):
    gradient = -(2 / len(y)) * X.T @ (y - X @ coefficients)
    coefficients -= learning_rate * gradient


# The first element of 'coefficients' is the intercept term, and the rest are the characteristic coefficients
intercept, feature_coefficients = coefficients[0], coefficients[1:]

print("\nintercept GD", intercept)
print("feature_coefficients GD", feature_coefficients)
