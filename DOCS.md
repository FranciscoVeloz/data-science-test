# Explicación examen data science

Note: This DOCS file was created as a notes file for understand and learn how to solve the exam, for that reason is in my native language, for understand how can I make the test, so, you are able to ignore this file.

## 1. Take MINST dataset. Implement Logistic regression to predict number from Handwritten digits

El enunciado se refiere a la implementación de la regresión logística para predecir números a partir de dígitos escritos a mano utilizando el conjunto de datos MNIST. Aquí tienes una explicación más detallada de cada uno de los componentes del enunciado:

- MNIST dataset: El MNIST dataset es un conjunto de datos ampliamente utilizado en el campo del aprendizaje automático y la visión por computadora. Contiene imágenes de dígitos escritos a mano del 0 al 9, y cada imagen tiene un tamaño de 28x28 píxeles en escala de grises. El conjunto de datos se utiliza comúnmente como un punto de partida para tareas de clasificación de imágenes y reconocimiento de dígitos.

- Implementar la regresión logística: La regresión logística es un algoritmo de aprendizaje supervisado que se utiliza para problemas de clasificación binaria o multiclase. En este caso, estás utilizando la regresión logística para predecir a qué número corresponde cada imagen del conjunto de datos MNIST. La regresión logística es un buen punto de partida para problemas de clasificación antes de explorar algoritmos más complejos, como las redes neuronales.

- Predecir números a partir de dígitos escritos a mano: Esto significa que el objetivo es entrenar un modelo de regresión logística que pueda tomar una imagen de un dígito escrito a mano como entrada y predecir qué número representa esa imagen. Por ejemplo, si se le da una imagen de un "3" escrito a mano, el modelo debe predecir que el número es "3".

Puedes cargar los datos del conjunto de datos MNIST y prepararlos en Python utilizando la biblioteca scikit-learn. Sin embargo, ten en cuenta que el conjunto de datos MNIST no se encuentra en scikit-learn de forma predeterminada. Para cargar el conjunto de datos MNIST, puedes utilizar la biblioteca fetch_openml, que permite acceder a una amplia variedad de conjuntos de datos, incluido MNIST. Aquí tienes los pasos para cargar los datos y prepararlos:

1. Instalar las bibliotecas necesarias:

Asegúrate de tener instaladas las siguientes bibliotecas: scikit-learn, numpy y matplotlib. Puedes instalarlas utilizando pip si aún no están instaladas:

```python
pip3 install scikit-learn numpy matplotlib
```

2. Cargar los datos MNIST:

```python
from sklearn.datasets import fetch_openml

# Cargar el conjunto de datos MNIST
mnist = fetch_openml('mnist_784', version=1, as_frame=False)
```

Este código descargará el conjunto de datos MNIST, que contiene las imágenes y las etiquetas asociadas. as_frame=False se utiliza para asegurarse de que los datos se carguen en un formato no estructurado (matrices numpy) en lugar de un DataFrame de pandas.

3. Preparar los datos:

Una vez que hayas cargado los datos, debes realizar las siguientes tareas de preparación:

- Normalizar las imágenes: Escala los valores de píxeles para que estén en un rango entre 0 y 1. Puedes hacerlo dividiendo cada valor de píxel por 255, ya que las imágenes están en escala de grises y los valores van de 0 (negro) a 255 (blanco).

```python
X = mnist.data / 255.0
```

- Convertir las etiquetas a valores numéricos: Las etiquetas en el conjunto de datos MNIST están en formato de cadena (por ejemplo, '0', '1', '2'). Debes convertirlas a valores numéricos enteros.

```python
y = mnist.target.astype(int)
```

4. Dividir los datos en conjuntos de entrenamiento y prueba:

Puedes utilizar la función train_test_split de scikit-learn para dividir los datos en conjuntos de entrenamiento y prueba. Por ejemplo:

```python
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
```

Esto dividirá los datos en un 80% para entrenamiento (X_train y y_train) y un 20% para prueba (X_test y y_test).

Con estos pasos, habrás cargado y preparado tus datos del conjunto de datos MNIST en Python para su uso en la implementación de la regresión logística y la validación cruzada.

## Use 10 fold cross validation

La actividad "Use 10-fold cross-validation" se refiere a una técnica de validación cruzada que se utiliza para evaluar el rendimiento de un modelo de machine learning de manera más robusta y precisa. La validación cruzada es una forma de dividir tus datos en conjuntos de entrenamiento y prueba de manera repetitiva para evaluar cómo se desempeña tu modelo en diferentes particiones de los datos.

En este caso, "10-fold cross-validation" significa que dividirás tus datos en 10 partes (o "folds") aproximadamente iguales. Luego, realizarás el siguiente proceso:

1. **División de datos**: Divide tu conjunto de datos en 10 partes iguales (folds).

2. **Iteraciones**: Realiza 10 iteraciones o rondas. En cada iteración, una de las 10 partes se usará como conjunto de prueba (datos de prueba), mientras que las otras 9 partes se utilizarán como conjunto de entrenamiento (datos de entrenamiento).

3. **Entrenamiento y evaluación**: Entrena tu modelo de regresión logística en los datos de entrenamiento y evalúa su rendimiento en los datos de prueba en cada iteración. Calcula una métrica de rendimiento, como la precisión (accuracy), en cada iteración.

4. **Promedio de métricas**: Después de las 10 iteraciones, obtendrás 10 valores de métricas de rendimiento, uno para cada iteración. Puedes calcular el promedio de estas métricas para obtener una estimación general del rendimiento de tu modelo.

La ventaja de utilizar la validación cruzada de 10-fold (o k-fold, donde k es el número de folds) es que te permite evaluar tu modelo en múltiples divisiones de los datos, lo que puede ayudar a detectar problemas de sobreajuste o subajuste. Además, proporciona una estimación más confiable del rendimiento del modelo en datos no vistos.

En Python, puedes realizar 10-fold cross-validation utilizando bibliotecas como scikit-learn. Aquí tienes un ejemplo de cómo hacerlo:

```python
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression

# Cargar tus datos y prepararlos como se mencionó anteriormente

# Crear un modelo de regresión logística
model = LogisticRegression()
model.fit(X_train, y_train)

# Realizar 10-fold cross-validation y calcular la precisión
scores = cross_val_score(model, X_train, y_train, cv=10, scoring='accuracy')

# Imprimir el promedio de las puntuaciones de precisión
print("Promedio de precisión: ", scores.mean())
```

En este ejemplo, `X_train` representa tus datos de entrenamiento y `y_train` las etiquetas correspondientes. `cross_val_score` se encarga de dividir los datos en 10 folds, entrenar y evaluar el modelo en cada fold, y luego devolver las puntuaciones de precisión para cada iteración. Finalmente, calculamos el promedio de las puntuaciones de precisión.

## Create a 5/not-5 binary classifier.

La actividad "Create a 5/not-5 binary classifier" se refiere a la creación de un clasificador binario que pueda distinguir entre los dígitos "5" y "no-5" en el conjunto de datos MNIST. En otras palabras, se trata de un problema de clasificación binaria donde el objetivo es predecir si una imagen contiene el número "5" o no. Los subpuntos de esta actividad se centran en ajustar el umbral de decisión del clasificador para mejorar la precisión y el recall, así como en visualizar y analizar los resultados utilizando una matriz de confusión y una curva de precisión-recall.

Vamos a abordar cada subpunto:

1. Cambiar el umbral para mejorar la precisión (Change threshold to improve precision):

Para cambiar el umbral de decisión y mejorar la precisión, debes comprender cómo funciona el clasificador binario y cómo se relaciona con el umbral. Puedes ajustar el umbral para cambiar el equilibrio entre la precisión y el recall del modelo. Para aumentar la precisión, puedes incrementar el umbral. Puedes hacer esto utilizando la función de decisión del modelo y comparándola con un umbral personalizado.

```python
# Supongamos que 'model' es tu modelo de clasificación
y_scores = model.decision_function(X_test)

# Establece un umbral personalizado
custom_threshold = 0.5  # Por ejemplo, 0.5 es el umbral predeterminado

# Aplica el umbral personalizado
y_pred_custom = (y_scores > custom_threshold)
```

2. Cambiar el umbral para mejorar el recall (Change threshold to improve recall):

Al igual que en el punto anterior, puedes cambiar el umbral, pero esta vez para mejorar el recall. Para aumentar el recall, puedes reducir el umbral.

```python
# Supongamos que 'model' es tu modelo de clasificación
y_scores = model.decision_function(X_test)

# Establece un umbral personalizado más bajo
custom_threshold = 0.2  # Por ejemplo, un umbral más bajo

# Aplica el umbral personalizado
y_pred_custom = (y_scores > custom_threshold)
```

3. Dibujar la curva de precisión-recall (Draw precision-recall curve):

Para dibujar la curva de precisión-recall, necesitas calcular la precisión y el recall en diferentes umbrales de decisión y graficarlos en un gráfico. Puedes usar la función precision_recall_curve de scikit-learn para esto:

```python
from sklearn.metrics import precision_recall_curve
import matplotlib.pyplot as plt

precision, recall, thresholds = precision_recall_curve(y_test, y_scores)

plt.plot(recall, precision, marker='.')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Precision-Recall Curve')
plt.grid(True)
plt.savefig('./imgs/curve.png')
plt.close()
```

4. Utilizar la matriz de confusión antes y después de cambiar el umbral (Use confusion matrix before and after changing threshold, Explain why/how different values are changing):

La matriz de confusión muestra la cantidad de verdaderos positivos (TP), falsos positivos (FP), verdaderos negativos (TN) y falsos negativos (FN) de un modelo de clasificación. Puedes calcular la matriz de confusión tanto antes como después de cambiar el umbral y analizar cómo cambian los valores en la matriz.

```python
from sklearn.metrics import confusion_matrix

# Hacemos binario y_test
y_test_binary = (y_test == 5)

# Matriz de confusión antes de cambiar el umbral
conf_matrix_before = confusion_matrix(y_test_binary[:10], y_pred_original)

# Matriz de confusión después de cambiar el umbral
conf_matrix_after = confusion_matrix(y_test_binary[:10], y_pred_custom)
```

Para explicar por qué y cómo cambian los valores en la matriz de confusión, puedes comparar la precisión y el recall antes y después del cambio de umbral. Por lo general, un aumento en el umbral aumenta la precisión y disminuye el recall, mientras que una disminución en el umbral hace lo contrario.

Estos pasos te permitirán crear un clasificador binario para distinguir entre "5" y "no-5", ajustar el umbral para mejorar la precisión y el recall, visualizar la curva de precisión-recall y analizar la matriz de confusión antes y después del cambio de umbral. Esto te ayudará a comprender cómo el ajuste del umbral afecta el rendimiento del modelo en tareas de clasificación binaria.

## Create a multi class classifier using logistic regression

El enunciado se refiere a la creación de un clasificador multicategoría (multi class) utilizando regresión logística. La regresión logística es un clasificador binario, lo que significa que normalmente se utiliza para resolver problemas de clasificación con dos clases (por ejemplo, "clase 0" y "clase 1"). Sin embargo, cuando se tiene un problema de clasificación con más de dos clases, como reconocimiento de dígitos del 0 al 9, es necesario adaptar la regresión logística para que pueda manejar múltiples categorías.

Para hacer esto, existen dos enfoques comunes:

One-vs-One (OvO): En este enfoque, se entrena un clasificador binario para cada par de clases posible. Por ejemplo, para clasificar dígitos del 0 al 9, se entrenarían clasificadores para distinguir "0 vs. 1", "0 vs. 2", "0 vs. 3", ..., "8 vs. 9". Luego, se elige la clase que obtuvo la mayor cantidad de votos entre todos los clasificadores para una muestra de entrada.

One-vs-All (OvA o One-vs-Rest): En este enfoque, se entrena un clasificador binario para cada clase, donde la clase objetivo se considera como la "clase positiva" y todas las demás clases se agrupan como la "clase negativa". Por ejemplo, para clasificar dígitos del 0 al 9, se entrenarían 10 clasificadores, uno para cada dígito, donde cada clasificador decide si una muestra pertenece a su dígito objetivo o no.

Aquí te explico cómo implementar ambos enfoques en Python utilizando la regresión logística y cómo compararlos:

Implementación de One-vs-One (OvO):

```python
from sklearn.multiclass import OneVsOneClassifier
from sklearn.linear_model import LogisticRegression

# Crear un modelo de regresión logística
base_classifier = LogisticRegression()

# Crear un clasificador OvO
ovo_classifier = OneVsOneClassifier(base_classifier)

# Entrenar el clasificador OvO en tus datos (X_train y y_train)
ovo_classifier.fit(X_train, y_train)

# Realizar predicciones
y_pred_ovo = ovo_classifier.predict(X_test)

# Calcular la matriz de confusión y la precisión
from sklearn.metrics import confusion_matrix, accuracy_score

conf_matrix_ovo = confusion_matrix(y_test, y_pred_ovo)
accuracy_ovo = accuracy_score(y_test, y_pred_ovo)
```

Implementación de One-vs-All (OvA):

```python
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression

# Crear un modelo de regresión logística
base_classifier = LogisticRegression()

# Crear un clasificador OvA
ova_classifier = OneVsRestClassifier(base_classifier)

# Entrenar el clasificador OvA en tus datos (X_train y y_train)
ova_classifier.fit(X_train, y_train)

# Realizar predicciones
y_pred_ova = ova_classifier.predict(X_test)

# Calcular la matriz de confusión y la precisión
from sklearn.metrics import confusion_matrix, accuracy_score
conf_matrix_ova = confusion_matrix(y_test, y_pred_ova)
accuracy_ova = accuracy_score(y_test, y_pred_ova)
```

Luego, puedes comparar las matrices de confusión y las precisiones obtenidas con cada enfoque para determinar cuál de los dos enfoques es mejor para tu problema de clasificación multicategoría. En general, ambos enfoques pueden funcionar bien, pero uno puede tener un rendimiento ligeramente mejor que el otro según las características de tu conjunto de datos.


## 2. Use Cellphone data set and implement linear regression to predict price of mobile phone.

El enunciado "Use Cellphone data set and implement linear regression to predict price of mobile phone" se refiere a una tarea de aprendizaje automático en la que se utiliza un conjunto de datos llamado "Cellphone data" para implementar un modelo de regresión lineal con el objetivo de predecir el precio de teléfonos móviles (celulares).

Aquí está la explicación de los elementos clave del enunciado:

1. **Cellphone data set**: Este es el conjunto de datos que proporciona información sobre teléfonos móviles. El conjunto de datos puede contener diversas características o atributos relacionados con los teléfonos móviles, como la marca, el modelo, las especificaciones técnicas, la antigüedad, la condición, etc. Además, debe incluir la variable objetivo que deseas predecir, que en este caso es el precio de los teléfonos móviles.

2. **Implementar linear regression**: Significa que debes construir y entrenar un modelo de regresión lineal utilizando el conjunto de datos de teléfonos móviles. La regresión lineal es un algoritmo de aprendizaje supervisado que se utiliza para predecir un valor numérico (en este caso, el precio) basado en un conjunto de características o variables predictoras.

3. **Predict price of mobile phone**: El objetivo final es utilizar el modelo de regresión lineal para realizar predicciones del precio de los teléfonos móviles con base en las características proporcionadas en el conjunto de datos.

El flujo general de trabajo para esta tarea sería el siguiente:

- Cargar el conjunto de datos "Cellphone data" en un entorno de programación como Python.
- Explorar y preprocesar los datos, lo que incluye la limpieza de datos, la selección de características relevantes y la división de los datos en conjuntos de entrenamiento y prueba.
- Construir un modelo de regresión lineal utilizando los datos de entrenamiento.
- Entrenar el modelo utilizando los datos de entrenamiento, lo que implica ajustar los parámetros del modelo para que se ajusten mejor a los datos.
- Evaluar el modelo utilizando los datos de prueba para medir su capacidad para predecir con precisión los precios de los teléfonos móviles.
- Utilizar el modelo entrenado para hacer predicciones de precios de teléfonos móviles basadas en nuevas características o datos no vistos previamente.

El objetivo final es tener un modelo de regresión lineal que pueda hacer predicciones precisas sobre los precios de los teléfonos móviles en función de las características proporcionadas en el conjunto de datos "Cellphone data".

## Implement linear regression using OLS and Gradient Descent

El enunciado "Implement linear regression using OLS and Gradient Descent" se refiere a la implementación de dos métodos diferentes para realizar regresión lineal, conocidos como Mínimos Cuadrados Ordinarios (OLS) y Descenso del Gradiente (Gradient Descent). Ambos métodos son utilizados para ajustar un modelo de regresión lineal a un conjunto de datos y predecir una variable dependiente (en este caso, el precio de los teléfonos móviles) en función de una o más variables independientes (características del teléfono móvil).

A continuación, te explicaré brevemente en qué consiste cada uno de estos métodos y cómo puedes implementarlos en Python:

Implementación de Linear Regression con Mínimos Cuadrados Ordinarios (OLS):

Mínimos Cuadrados Ordinarios es un método analítico para encontrar los coeficientes del modelo de regresión lineal de manera que minimicen la suma de los cuadrados de las diferencias entre las predicciones del modelo y los valores reales. En Python, puedes utilizar la biblioteca numpy para realizar los cálculos. Aquí hay un ejemplo de cómo hacerlo:

```python
import numpy as np

# Supongamos que X es una matriz de características y y es el vector de etiquetas (precios)
X = ...  # Inserta tus datos de características aquí
y = ...  # Inserta tus etiquetas (precios) aquí

# Agregar una columna de unos para el término de intercepción
X = np.c_[np.ones(X.shape[0]), X]

# Calcular los coeficientes del modelo utilizando OLS
coefficients = np.linalg.inv(X.T @ X) @ X.T @ y

# El primer elemento de 'coefficients' es el término de intercepción, y el resto son los coeficientes de las características
intercept, feature_coefficients = coefficients[0], coefficients[1:]

# Ahora tienes los coeficientes para tu modelo de regresión lineal
```

Implementación de Linear Regression con Gradient Descent:

El Descenso del Gradiente es un método numérico para encontrar los coeficientes del modelo de regresión lineal ajustando iterativamente los parámetros en la dirección opuesta al gradiente de la función de costo. Puedes implementar Gradient Descent en Python de la siguiente manera:

```python
# Supongamos que X es una matriz de características y y es el vector de etiquetas (precios)
X = ...  # Inserta tus datos de características aquí
y = ...  # Inserta tus etiquetas (precios) aquí

# Agregar una columna de unos para el término de intercepción
X = np.c_[np.ones(X.shape[0]), X]

# Definir hiperparámetros
learning_rate = 0.01
num_iterations = 1000

# Inicializar coeficientes aleatoriamente o con ceros
coefficients = np.random.rand(X.shape[1])

# Implementar el Descenso del Gradiente
for iteration in range(num_iterations):
    gradient = -(2 / len(y)) * X.T @ (y - X @ coefficients)
    coefficients -= learning_rate * gradient

# El primer elemento de 'coefficients' es el término de intercepción, y el resto son los coeficientes de las características
intercept, feature_coefficients = coefficients[0], coefficients[1:]

# Ahora tienes los coeficientes para tu modelo de regresión lineal ajustado con Gradient Descent
```

Ten en cuenta que en ambos casos, se asume que ya tienes tus datos de características (X) y etiquetas (y) cargados y preparados. Además, es importante ajustar los hiperparámetros, como la tasa de aprendizaje (learning_rate) y el número de iteraciones (num_iterations), según las necesidades de tu problema. Después de ejecutar estos fragmentos de código, tendrás los coeficientes de tu modelo de regresión lineal que puedes utilizar para hacer predicciones de precios de teléfonos móviles.
