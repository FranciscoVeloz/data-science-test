import matplotlib.pyplot as plt
from sklearn.datasets import fetch_openml
from sklearn.multiclass import OneVsOneClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_curve, confusion_matrix, accuracy_score


### Take MINST dataset. Implement Logistic regression to predict number from Handwritten digits
mnist = fetch_openml('mnist_784', version=1, as_frame=False)
X = mnist.data / 255.0
y = mnist.target.astype(int)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

## a. Use 10 fold cross validation
model = LogisticRegression()
model.fit(X_train, y_train)
scores = cross_val_score(model, X_train, y_train, cv=10, scoring='accuracy')

print("Promedio de precisión: ", scores.mean())

## b. Create a 5/not-5 binary classifier
y_scores = model.decision_function(X_test)
y_test_binary = (y_test == 5)

# Confusion matrix before change threshold
default_threshold = 0.5
y_pred_original = (y_scores > default_threshold)

conf_matrix_before = confusion_matrix(y_test_binary[:10], y_pred_original[0])
print("Confusion matrix before: ", conf_matrix_before)

# Change threshold to improve precision
custom_threshold = 0.8
y_pred_custom = (y_scores > custom_threshold)

# Confusion matrix after change threshold
conf_matrix_after = confusion_matrix(y_test_binary[:10], y_pred_original[0])
print("Confusion matrix after: ", conf_matrix_after)

# Change threshold to improve recall
custom_threshold = 0.2
y_pred_custom = (y_scores > custom_threshold)

# Confusion matrix after change threshold
conf_matrix_after = confusion_matrix(y_test_binary[:10], y_pred_custom[0])
print("Confusion matrix after: ", conf_matrix_after)

#Draw precision-recall curve
y_scores_binary = (y_scores == 5)
precision, recall, thresholds = precision_recall_curve(y_test_binary[:10], y_scores_binary[0][:10])

plt.plot(recall, precision, marker='.')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Precision-Recall Curve')
plt.grid(True)
plt.savefig('./imgs/curve.png')
plt.close()

## c. Create a multi class classifier using logistic regression
base_classifier = LogisticRegression()

# OvO classifier
ovo_classifier = OneVsOneClassifier(base_classifier)

# Training OvO classifier
ovo_classifier.fit(X_train, y_train)

# Make predictions
y_pred_ovo = ovo_classifier.predict(X_test)

conf_matrix_ovo = confusion_matrix(y_test, y_pred_ovo)
accuracy_ovo = accuracy_score(y_test, y_pred_ovo)

print ("One vs One", conf_matrix_ovo)
print ("accuracy_ovo", accuracy_ovo)

# OvA classifier
ova_classifier = OneVsRestClassifier(base_classifier)

# Training OvA classifier
ova_classifier.fit(X_train, y_train)

# Make predictions
y_pred_ova = ova_classifier.predict(X_test)

conf_matrix_ova = confusion_matrix(y_test, y_pred_ova)
accuracy_ova = accuracy_score(y_test, y_pred_ova)

print ("One vs All", conf_matrix_ova)
print ("accuracy_ovo", accuracy_ova)